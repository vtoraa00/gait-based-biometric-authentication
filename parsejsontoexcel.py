import json 
import os
import pandas as pd
import xlsxwriter



directory = r'C:\XX\XX\XX\Subject_XX_YY_ZZZZZZZZZZZZ_keypoints.json'
row = 1
col = 0
label = 0 #Definir número de etiqueta
num_file=0 #Definir fichero
num_concat=0 #Definir número de frames a concatenar 
num_jsons=0 
heap=1 #Definir salto

keypoints_dict= []

workbook = xlsxwriter.Workbook('./Excels/output.xlsx')
worksheet = workbook.add_worksheet()

title = ['NarizX', 'NarizY', 'CuelloX', 'CuelloY', 'HombroDerX', 'HombroDerY', 'CodoDerX', 'CodoDerY', 'MunecaDerX', 'MunecaDerY', 'HombroIzqX', 'HombroIzqY', 'CodoIzqX', 'CodoIzqY', 'MunecaIzqX', 'MunecaIzqY', 'CaderaMediaX', 'CaderaMediaY','CaderaDerX', 'CaderaDerY', 'RodillaDerX', 'RodillaDerY', 'TobilloDerX', 'TobilloDerY', 'CaderaIzqX', 'CaderaIzqY', 'RodillaIzqX', 'RodillaIzqY', 'TobilloIzqX', 'TobilloIzqY', 'OjoDerX', 'OjoDerY', 'OjoIzqX', 'OjoIzqY', 'OrejaDerX', 'OrejaDerY', 'OrejaIzqX', 'OrejaIzqY', 'Dedo1IzqX', 'Dedo1IzqY', 'Dedo5IzqX', 'Dedo5IzqY', 'TalonIzqX', 'TalonIzqY', 'Dedo1DerX', 'Dedo1DerY', 'Dedo5DerX', 'Dedo5DerY', 'TalonDerX', 'TalonDerY']
i=0

for x in range(num_concat):
    for j,e in enumerate(title):
        name = e+"_"+str(x) 
        worksheet.write(0,i,name)
        i=i+1
worksheet.write(0,i,"Label")



for entry in os.scandir(directory):
     
    num_jsons+=1
    with open(entry) as json_file:
        keypoint_list[:] = []
        ## editar json
        data = json.load(json_file)
        data2 = data["people"]
        data2_str = str(data2)
        data3 = data2_str.split(": [")
        if (len(data3)>2):
            data4 = data3[2]
            data5 = data4.split("],")
            keypoints = data5[0].split(",")
           
   
        keypoints_dict.append(keypoints)
   

x=0
while x < num_jsons:
    
    num_file = num_file+1
    
    
    if(heap % 2 !=0):
        for y in range(len(keypoints_dict[x])):
            if ((y+1)%3!=0):
                worksheet.write(row,col,keypoints_dict[x][y])
                col=col+1
    x+=1
    heap+=1
    
    
    if num_file == num_concat*2:
        worksheet.write(row,col,label)
        row=row+1
        col = 0
        num_file=0   
        x = x-(num_concat*2-1)
        

 
workbook.close()



